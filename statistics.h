#ifndef STATISTICS_H
#define STATISTICS_H

#include <QDialog>
#include "mainwindow.h"
#include <QtCharts>

namespace Ui {
class Statistics;
}

class Statistics : public QDialog
{
    Q_OBJECT
private:
    QChart *createPieChart() const;

public:
    explicit Statistics(QWidget *parent = nullptr, QList<struct analyzeitem> = {});
    ~Statistics();

private:
    Ui::Statistics *ui;
\
};

#endif // STATISTICS_H
