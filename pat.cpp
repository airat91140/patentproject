#include "pat.h"
#include "ui_pat.h"

pat::pat(QWidget *parent, QString text) :
    QDialog(parent),
    ui(new Ui::pat)
{
    ui->setupUi(this);
    ui->textBrowser->setPlainText(text);

}

pat::~pat()
{
    delete ui;
}
