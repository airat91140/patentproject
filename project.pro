QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

CONFIG += c++14

CONFIG += no_keywords

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    author.cpp \
    dates.cpp \
    main.cpp \
    mainwindow.cpp \
    pat.cpp \
    statistics.cpp

HEADERS += \
    author.h \
    dates.h \
    mainwindow.h \
    pat.h \
    statistics.h

FORMS += \
    author.ui \
    dates.ui \
    mainwindow.ui \
    pat.ui \
    statistics.ui

QT += xml
QT += charts

unix|win32: LIBS += -lclucene-shared-static

unix|win32: LIBS += -lclucene-core-static

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../clucene/buildname/bin/release/ -lclucene-core-static
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../clucene/buildname/bin/debug/ -lclucene-core-static
else:unix: LIBS += -L$$PWD/../clucene/buildname/bin/ -lclucene-core-static

INCLUDEPATH += $$PWD/../clucene/buildname/bin
DEPENDPATH += $$PWD/../clucene/buildname/bin

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/release/libclucene-core-static.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/debug/libclucene-core-static.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/release/clucene-core-static.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/debug/clucene-core-static.lib
else:unix: PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/libclucene-core-static.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../clucene/buildname/bin/release/ -lclucene-shared-static
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../clucene/buildname/bin/debug/ -lclucene-shared-static
else:unix: LIBS += -L$$PWD/../clucene/buildname/bin/ -lclucene-shared-static

INCLUDEPATH += $$PWD/../clucene/buildname/bin
DEPENDPATH += $$PWD/../clucene/buildname/bin

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/release/libclucene-shared-static.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/debug/libclucene-shared-static.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/release/clucene-shared-static.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/debug/clucene-shared-static.lib
else:unix: PRE_TARGETDEPS += $$PWD/../clucene/buildname/bin/libclucene-shared-static.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/release/ -lclucene-contribs-lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/debug/ -lclucene-contribs-lib
else:unix: LIBS += -L$$PWD/../clucene/anotherbuild/bin/ -lclucene-contribs-lib

INCLUDEPATH += $$PWD/../clucene/anotherbuild/bin
DEPENDPATH += $$PWD/../clucene/anotherbuild/bin

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/release/ -lclucene-core
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/debug/ -lclucene-core
else:unix: LIBS += -L$$PWD/../clucene/anotherbuild/bin/ -lclucene-core

INCLUDEPATH += $$PWD/../clucene/anotherbuild/bin
DEPENDPATH += $$PWD/../clucene/anotherbuild/bin

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/release/ -lclucene-shared
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../clucene/anotherbuild/bin/debug/ -lclucene-shared
else:unix: LIBS += -L$$PWD/../clucene/anotherbuild/bin/ -lclucene-shared

INCLUDEPATH += $$PWD/../clucene/anotherbuild/bin
DEPENDPATH += $$PWD/../clucene/anotherbuild/bin

unix: CONFIG += link_pkgconfig
