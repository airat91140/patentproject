#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtXml>
#include <QString>
#include <QDirIterator>
#include <QXmlStreamAttribute>
#include <QMessageBox>
#include <QRegExp>
#include "pat.h"
#include "statistics.h"
#include "dates.h"
#include <stdio.h>
#define MIN 0
using namespace std;

typedef struct item {
    int amount;
    QDate date;
} item;

struct searchitem {
    QString str;
    QString fn;
    float per;
};

struct analyzeitem {
    QString str;
    int amount;
};

QList<struct searchitem> results = {};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString showFullXml(QString filename)
{
    QString name;
    QFile f(filename);
    QString str = "";
    f.open(QIODevice::ReadOnly);
    QXmlStreamReader xml;
    xml.setDevice(&f);
    xml.readNext();
    while(!(xml.atEnd() && xml.hasError())) {
        xml.readNext();
        if(xml.isStartElement()){
            name = xml.name().toString();
            if(name == "ru-name")
                str.append("Правообладатель: ");
            else if(name == "ru-country")
                str.append("Страна: ");
            else if(name == "ru-full-name")
                str.append("Авторы: ");
            else if(name == "ru-program-name")
                str.append("Название: ");
            else if(name == "ru-scope")
                str.append("Реферат: ");
            else if(name == "ru-language")
                str.append("Язык программирования: ");
            else if(name == "ru-features")
                str.append("Тип ЭВМ: ");
            else if(name == "ru-os")
                str.append("Тип ОС: ");
            else if(name == "ru-size")
                str.append("Объём программы: ");
            else if(name == "B110")
                str.append("Номер регистрации: ");
            else if(name == "B130")
                str.append("Тип рида: ");
            else if(name == "B140") {
                str.append("Дата регистрации: ");
                xml.readNext();
            }
            else if(name == "B190")
                str.append("Место регистрации: ");
            else if(name == "B210")
                str.append("Номер и дата поступления заявки");
            else continue;
            str.append(xml.readElementText());
            str.append("\n");
        }
    }
    f.close();
    name.clear();
    return str;
}

bool listSearch(QString text, QList<analyzeitem> list, int *n) {
    for (int i = 0; i < list.length(); ++i)
        if (list[i].str.toLower() == text.toLower()) {
            *n = i;
            return true;
        }
    return false;
}

QStringList canonize(QString str)
{
    QStringList newstr;
    QStringList stopwords = {"а", "на базе", "у", "к", "в", "с", "и", "или", "через", "над", "из", "под",
                             "из", "за", "из", "под", "не", "но", "же", "то", "да", "ли", "для"};
    str = str.simplified();
    str = str.toLower();
    newstr = str.split(QRegExp("[.,!?:;\'\"\n\r() \t]"), QString::SkipEmptyParts);
    for(QStringList::iterator i = stopwords.begin(); i != stopwords.end(); ++i){
        QString &word = *i;
        if (newstr.contains(word)) {
            newstr.removeAll(word);
        }
    }
    return newstr;
}

QList<int> genshingle(QStringList list, int shingleLen)
{
    QString str;
    if (shingleLen > list.length())
        shingleLen = list.length();
    QList<int> out;
            out = {};
    for (int i = 0; i < (list.length() - (shingleLen - 1)); ++i) {
            str.clear();
            for (int j = i; j < i + shingleLen; ++j) {
                str.append(list[j]);
            }
            out.append(qHash(str));
        }
    return out;
}

float compare(QList<int> source1,QList<int> source2)
{
    float same = 0;
    for (int i = 0; i < source1.length(); ++i) {
        if (source2.contains(source1[i]))
            ++same;
    }
    return same * 2 / (float)(source1.length() + (source2.length())) * 100;
}

void qs(QList<struct searchitem> *s_arr, int first, int last)
{
    if (first < last)
    {
        int left = first, right = last;
        float middle = (*s_arr)[(left + right) / 2].per;
        do
        {
            while ((*s_arr)[left].per > middle) left++;
            while ((*s_arr)[right].per < middle) right--;
            if (left <= right)
            {
                struct searchitem tmp = (*s_arr)[left];
                (*s_arr)[left] = (*s_arr)[right];
                (*s_arr)[right] = tmp;
                left++;
                right--;
            }
        } while (left <= right);
        qs(s_arr, first, right);
        qs(s_arr, left, last);
    }
}

void qsort(QList<struct analyzeitem> *s_arr, int first, int last)
{
    if (first < last)
    {
        int left = first, right = last;
        int middle = (*s_arr)[(left + right) / 2].amount;
        do
        {
            while ((*s_arr)[left].amount > middle) left++;
            while ((*s_arr)[right].amount < middle) right--;
            if (left <= right)
            {
                struct analyzeitem tmp = (*s_arr)[left];
                (*s_arr)[left] = (*s_arr)[right];
                (*s_arr)[right] = tmp;
                left++;
                right--;
            }
        } while (left <= right);
        qsort(s_arr, first, right);
        qsort(s_arr, left, last);
    }
}

QString showXml(QString filename)
{
    QFile f(filename);
    f.open(QIODevice::ReadOnly);
    QXmlStreamReader xml;
    xml.setDevice(&f);
    QString str;
    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement()) {
            if (xml.name() == "ru-full-name" || xml.name() == "ru-program-name") {
                str.append(xml.readElementText());
                str.append('\n');
            }
        }
    }
    f.close();
    return str;
}

QList<struct searchitem> MainWindow::Search(QString text)
{
    QList<struct searchitem> result = {};
    QStringList names = {};
    QDirIterator it("./xmls", QDirIterator::Subdirectories);
    float per;
    if (ui->AuthorcheckBox->isChecked())
        names.append("ru-full-name");
    if (ui->NamecheckBox->isChecked())
        names.append("ru-program-name");
    if (ui->DescrcheckBox->isChecked())
        names.append("ru-scope");
    while (it.hasNext()) {
        QFile f(it.next());
        if (f.fileName().endsWith("..") || f.fileName().endsWith("."))
            continue;
        f.open(QIODevice::ReadOnly);
        QXmlStreamReader xml;
        xml.setDevice(&f);
        while (!(xml.atEnd() && xml.hasError())) {
            int flag = 0;
            xml.readNext();
            if(xml.isStartElement()){
                if(names.contains(xml.name())) {
                    QList<float> percents = {};
                    QString source = xml.readElementText();
                    for (int i = 1; i > 0; --i)
                        if ((per = compare(genshingle(canonize(source), i),
                                           genshingle(canonize(text), i))) > MIN) {
                            result.append({"", f.fileName(), per});
                            flag = 1;
                            break;
                        }
                    if(flag == 1)
                        break;
                }
            }
        }
        f.close();
    }
    return result;
}

QList<struct searchitem> MainWindow::Searchinto(QString text)
{
    QStringList names;
    QList<struct searchitem> result = {};
    float per;
    if (ui->AuthorcheckBox->isChecked())
        names.append("ru-full-name");
    if (ui->NamecheckBox->isChecked())
        names.append("ru-program-name");
    if (ui->DescrcheckBox->isChecked())
        names.append("ru-scope");
    for(int i = 0; i < results.length(); ++i){
        QFile f(results[i].fn);
        f.open(QIODevice::ReadOnly);
        QXmlStreamReader xml;
        xml.setDevice(&f);
        while (!(xml.atEnd() && xml.hasError())) {
            int flag = 0;
            xml.readNext();
            if(xml.isStartElement()){
                if(names.contains(xml.name())) {
                    QList<float> percents = {};
                    QString source = xml.readElementText();
                    for (int i = 1; i > 0; --i)
                        if ((per = compare(genshingle(canonize(source), i),
                                           genshingle(canonize(text), i))) > MIN) {
                            result.append({"", f.fileName(), per});
                            flag = 1;
                            break;
                        }
                    if(flag ==1)
                        break;
                }
            }
        }
        f.close();
    }
    results.clear();
    return result;
}

QList<struct analyzeitem> DateAnalyze()
{
    int n;
    QList<struct analyzeitem> result = {};
    QString tmp;
    QDirIterator it("./xmls", QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QFile f(it.next());
        if (f.fileName().endsWith("..") || f.fileName().endsWith("."))
            continue;
        f.open(QIODevice::ReadOnly);
        QXmlStreamReader xml;
        xml.setDevice(&f);
        while (!(xml.atEnd() && xml.hasError())) {
            xml.readNext();
            if(xml.isStartElement()){
                if(xml.name() == "B140"){
                    xml.readNext();
                    tmp = xml.readElementText();
                    tmp.remove(7,3);
                    if (!listSearch(tmp, result, &n)){
                        result.append({tmp, 1});
                        break;
                    }
                    else
                        ++result[n].amount;
                }
            }
        }

        f.close();
    }
    return result;
}

QList<struct analyzeitem> MainWindow::LangAnalyze()
{
    int n;
    QList<struct analyzeitem> result = {};
    QStringList tmplist;
    QDirIterator it("./xmls", QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QFile f(it.next());
        if (f.fileName().endsWith("..") || f.fileName().endsWith("."))
            continue;
        f.open(QIODevice::ReadOnly);
        QXmlStreamReader xml;
        xml.setDevice(&f);
        while (!(xml.atEnd() && xml.hasError())) {
            xml.readNext();
            if(xml.isStartElement()){
                if(xml.name() == "ru-language") {
                    QString source = xml.readElementText();
                    tmplist = source.split(QRegExp("[),;\(]"), QString::SkipEmptyParts);
                    for(int i = 0; i < tmplist.length(); ++i) {
                        source = tmplist[i];
                        source = source.trimmed();
                        source.replace("С", "C");
                        source.replace("А", "A");
                        source.replace("а", "A");
                        source.replace("Р", "P");
                        source.replace("р", "P");
                        source.replace("Н", "H");
                        source.replace("н", "H");
                        source.replace("У", "Y");
                        source.replace("у", "Y");
                        source.replace("К", "K");
                        source.replace("к", "K");
                        source.replace("В", "B");
                        source.replace("в", "B");
                        source.replace("О", "O");
                        source.replace("о", "O");
                        source.replace("М", "M");
                        source.replace("м", "M");
                        source.replace("Т", "T");
                        source.replace("т", "T");
                        source.replace("Е", "E");
                        source.replace("е", "E");
                        source.replace(" script", "script");
                        source.replace("с", "C");
                        if (!listSearch(source, result, &n)){
                            result.append({source, 1});
                            break;
                        }
                        else
                            ++result[n].amount;
                    }
                }
            }
        }
        f.close();
    }
    qsort(&result,  0, result.length() - 1);
    FILE *f;
    f = fopen("lang.txt", "w+");
    for(int i = 0; i < result.length(); ++i){
        fprintf(f, "%s\t%d\n", result[i].str.toStdString().c_str(), result[i].amount);
    }
    fclose(f);
    return result;
}

void MainWindow::on_pushButton_search_clicked()
{
    QString text = ui->lineEdit->text();
    qDebug() << text;
    if(ui->actionSearch_into->isChecked())
        results = Searchinto(text);
    else
        results = Search(text);
    if (results.isEmpty()) {
       QMessageBox::information(this, "Ошибка", "Ничего не найдено");
       return;
    }
    qs(&results, 0, results.length() - 1);
    for (int i = 0; i < results.length(); ++i) {
        results[i].str = showXml(results[i].fn);
    }
    QMessageBox::information(this, "Внмиание", "Найдено " + QString::number(results.length()) +" элементов");
    if(results.length() > 6){
        ui->textBrowser_1->setPlainText(results[0].str);
        ui->textBrowser_2->setPlainText(results[1].str);
        ui->textBrowser_3->setPlainText(results[2].str);
        ui->textBrowser_4->setPlainText(results[3].str);
        ui->textBrowser_5->setPlainText(results[4].str);
        ui->textBrowser_6->setPlainText(results[5].str);
    }
    else {
        while(results.length() != 6)
            results.append({"","",0});
        ui->textBrowser_1->setPlainText(results[0].str);
        ui->textBrowser_2->setPlainText(results[1].str);
        ui->textBrowser_3->setPlainText(results[2].str);
        ui->textBrowser_4->setPlainText(results[3].str);
        ui->textBrowser_5->setPlainText(results[4].str);
        ui->textBrowser_6->setPlainText(results[5].str);
    }
}

void MainWindow::on_pushButton_next_clicked()
{
    if(results.length() > 6){
        for (int i = 0; i < 6; ++i)
            results.removeFirst();
        ui->textBrowser_1->setPlainText(results[0].str);
        ui->textBrowser_2->setPlainText(results[1].str);
        ui->textBrowser_3->setPlainText(results[2].str);
        ui->textBrowser_4->setPlainText(results[3].str);
        ui->textBrowser_5->setPlainText(results[4].str);
        ui->textBrowser_6->setPlainText(results[5].str);
    }
    else {
        while(results.length() != 6)
            results.append({"","",0});
        ui->textBrowser_1->setPlainText(results[0].str);
        ui->textBrowser_2->setPlainText(results[1].str);
        ui->textBrowser_3->setPlainText(results[2].str);
        ui->textBrowser_4->setPlainText(results[3].str);
        ui->textBrowser_5->setPlainText(results[4].str);
        ui->textBrowser_6->setPlainText(results[5].str);
    }
}

void MainWindow::on_action_triggered()
{
    QApplication::quit();
}

void MainWindow::on_pushButtonOpen_1_clicked()
{
    pat *page = new pat(this, showFullXml(results[0].fn));
    page->show();
}

void MainWindow::on_pushButtonOpen_2_clicked()
{
    pat *page = new pat(this, showFullXml(results[1].fn));
    page->show();
}

void MainWindow::on_pushButtonOpen_3_clicked()
{
    pat *page = new pat(this, showFullXml(results[2].fn));
    page->show();
}

void MainWindow::on_pushButtonOpen_4_clicked()
{
    pat *page = new pat(this, showFullXml(results[3].fn));
    page->show();
}

void MainWindow::on_pushButtonOpen_5_clicked()
{
    pat *page = new pat(this, showFullXml(results[4].fn));
    page->show();
}

void MainWindow::on_pushButtonOpen_6_clicked()
{
    pat *page = new pat(this, showFullXml(results[5].fn));
    page->show();
}

void MainWindow::on_pushButton_analyze_clicked()
{
    if(ui->checkBox_language->isChecked()){
        Statistics *page = new Statistics(this, LangAnalyze());
        page->show();
    }
    if(ui->checkBox_date->isChecked()){
        dates *page = new dates(this, DateAnalyze());
        page->show();
    }
}
