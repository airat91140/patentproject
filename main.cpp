#include "mainwindow.h"
#include "pat.h"
#include "author.h"
#include "statistics.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
