#include "dates.h"
#include "ui_dates.h"
#include <QChart>
#include <QChartView>
#include "statistics.cpp"

typedef struct item {
    int amount;
    QDate date;
} item;

void qs(QList<item> *s_arr, int first, int last);

/*struct analyzeitem {
    QString str;
    int amount;
};*/

dates::dates(QWidget *parent, QList<struct analyzeitem> list) :
    QDialog(parent),
    ui(new Ui::dates)
{
    ui->setupUi(this);

    QList<item> stats;
    for(int i = 0; i < list.length(); ++i){
        stats.append({list[i].amount, QDate::fromString(list[i].str, "yyyy-MM")});
    }
    qs(&stats, 0, stats.length() - 1);
    FILE *logs;
    logs = fopen("logs.txt", "w");
    for(int i = 0; i < stats.length(); ++i){
        fprintf(logs, "%s %s\n", stats[i].date.toString("yyyy-MM").toStdString().c_str(),
                std::to_string(stats[i].amount).c_str());
    }
    fclose(logs);
    QChartView *chartView;
    QChart *chart = new QChart();
    chart->setTitle("Активность изобретателей");
    chart->legend()->show();
    chart->createDefaultAxes();
    QBarSeries *series = new QBarSeries(chart);
    QBarSet *set = new QBarSet("");
    QStringList categories;

    for(int i = 0; i < stats.length(); ++i) {
        categories << (stats[i].date.toString("yyyy-MM"));
        *set << stats[i].amount;
    }
    series->append(set);
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    chart->addSeries(series);
    chartView = new QChartView(chart);
    ui->gridLayout->addWidget(chartView);
}

void qs(QList<item> *s_arr, int first, int last)
{
    if (first < last)
    {
        int left = first, right = last;
        QDate middle = (*s_arr)[(left + right) / 2].date;
        do
        {
            while ((*s_arr)[left].date < middle) left++;
            while ((*s_arr)[right].date > middle) right--;
            if (left <= right)
            {
                struct item tmp = (*s_arr)[left];
                (*s_arr)[left] = (*s_arr)[right];
                (*s_arr)[right] = tmp;
                left++;
                right--;
            }
        } while (left <= right);
        qs(s_arr, first, right);
        qs(s_arr, left, last);
    }
}

dates::~dates()
{
    delete ui;
}
