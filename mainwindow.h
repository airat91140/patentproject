#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <string>
#include <pat.h>
#include <QObject>
#include <QXmlStreamReader>
#include <QtXml>
#include <QShortcut>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QList<struct searchitem> NameSearch(QString text);
    QList<struct searchitem> AuthorSearch(QString text);
    QList<struct searchitem> DescriptionSearch(QString text);
    QList<struct analyzeitem> LangAnalyze();
    QList<struct searchitem> Search(QString text);
    QList<struct searchitem> Searchinto(QString text);


private Q_SLOTS:

    void on_pushButton_search_clicked();

    void on_action_triggered();

    void on_pushButtonOpen_1_clicked();

    void on_pushButtonOpen_2_clicked();

    void on_pushButtonOpen_3_clicked();

    void on_pushButtonOpen_4_clicked();

    void on_pushButtonOpen_5_clicked();

    void on_pushButtonOpen_6_clicked();

    void on_pushButton_next_clicked();

    void on_pushButton_analyze_clicked();


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
