#ifndef PAT_H
#define PAT_H

#include <QDialog>

namespace Ui {
class pat;
}


class pat : public QDialog
{
    Q_OBJECT

public:
    explicit pat(QWidget *parent = nullptr, QString text = "");
    ~pat();

private:
    Ui::pat *ui;
};

#endif // PAT_H
