#include "statistics.h"
#include "ui_statistics.h"

struct analyzeitem {
    QString str;
    int amount;
};

Statistics::Statistics(QWidget *parent, QList<struct analyzeitem> list) :
    QDialog(parent),
     ui(new Ui::Statistics)
{
    ui->setupUi(this);
    int sum = 0;
    QChartView *chartView;
    QChart *chart = new QChart();
    chart->setTitle("Языки программирования");
    QPieSeries *series = new QPieSeries(chart);
    for(int i = 0; i < 10; ++i) {
        QPieSlice *slice = series->append(list[i].str + " " + QString::number(list[i].amount),
                                          list[i].amount);
        slice->setColor(QColor(QRandomGenerator::global()->generate() % 255,
                              QRandomGenerator::global()->generate() % 255,
                              QRandomGenerator::global()->generate() % 255));
        slice->setLabelVisible();
    }
    for (int j = 10; j < list.length(); ++j) {
        sum += list[j].amount;
    }
    QPieSlice *slice = series->append("Остальные " + QString::number(sum), sum);
    slice->setColor(QColor(QRandomGenerator::global()->generate() % 255,
                          QRandomGenerator::global()->generate() % 255,
                          QRandomGenerator::global()->generate() % 255));
    slice->setLabelVisible();
    series->setPieSize(0.85);
    chart->addSeries(series);
    chartView = new QChartView(chart);
    ui->gridLayout->addWidget(chartView);
}

Statistics::~Statistics()
{
    delete ui;
}
