#ifndef DATES_H
#define DATES_H

#include <QDialog>
#include <mainwindow.h>

namespace Ui {
class dates;
}

class dates : public QDialog
{
    Q_OBJECT

public:
    explicit dates(QWidget *parent = nullptr, QList<struct analyzeitem> = {});
    ~dates();

private:
    Ui::dates *ui;
};

#endif // DATES_H
